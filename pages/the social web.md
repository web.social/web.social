- In 2010, W3C's Social Web Incubator Group [published "A Standards-based, Open and Privacy-aware Social Web"](https://www.w3.org/2005/Incubator/socialweb/XGR-socialweb-20101206/)
  id:: 6466b43b-f4ab-4c33-b204-d44fe8d1f688
	- > **The Social Web is a set of relationships that link together people over 
	  the Web**. The Web is an universal and open space of information where 
	  every item of interest can be identified with a URI. While the best 
	  known current social networking sites on the Web limit themselves to 
	  relationships between people with accounts on a single site, **the Social 
	  Web should extend across the entire Web**. Just as people can call each 
	  other no matter which telephone provider they belong to, just as email 
	  allows people to send messages to each other irrespective of their 
	  e-mail provider, and just as the Web allows links to any website, so **the
	   Social Web should allow people to create networks of relationships 
	  across the entire Web, while giving people the ability to control their 
	  own privacy and data.** **The standards that enable this should be  open and
	   royalty-free.** We present a framework for understanding the Social Web 
	  and the relevant standards (from both within and outside the W3C) in 
	  this report, and conclude by proposing **a strategy for making the Social 
	  Web a "first-class citizen" of the Web.**
	-