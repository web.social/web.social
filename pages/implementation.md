- > **Implementation** is the realization of an application, or execution of a [plan](https://en.wikipedia.org/wiki/Plan), idea, [model](https://en.wikipedia.org/wiki/Scientific_modelling), [design](https://en.wikipedia.org/wiki/Design), [specification](https://en.wikipedia.org/wiki/Specification), [standard](https://en.wikipedia.org/wiki/Standardization), [algorithm](https://en.wikipedia.org/wiki/Algorithm), or [policy](https://en.wikipedia.org/wiki/Policy).
  
  via [wikipedia.org](https://en.wikipedia.org/wiki/Implementation)
	-
- [[implementations of [[the social web]]]]