- *web.social* is an [[implementation]] of [[the social web]].
  id:: 6466b433-a67c-4542-aafe-264ede0bcd90
- #web.social/playgroup is a group that plays with web.social
  id:: 6466b435-4693-497d-9de3-14b7cef72b7e
- ## Topics
  #protocols #[[the social web]] #design #implementation
- ## Parts
- #web.social/playgroup
- #web.social/design
- ## Questions
- [[What is web.social?]]
- [[What is the social web?]]
- ## Reports
- https://www.w3.org/TR/social-web-protocols/
	- > **The Social Web Protocols are a collection of standards which enable 
	  various aspects of decentralised social interaction on the Web.** This 
	  document describes the purposes of each, and how they fit together.
	- > People and the content they create are the core components of the 
	  social web; they make up the social graph. This document describes a 
	  standard way in which people can:
	  > * create, update and delete social content;
	  > * connect with other people by subscribing to their content;
	  > * interact with other peoples' content;
	  > * be notified when other people interact with their content.
	  > regardless of *what that content* is or *where it is stored*.
	  > 
	  > These components are core building blocks for interoperable decentralised social systems.
	  > 
	  > Each of these components can be implemented independently as needed, or all together in one system, as well as extended to meet domain-specific requirements. Users can store their social data across any number of compliant servers, and use compliant clients hosted elsewhere to interact with their own content and the content of others.
	  >
	  > The [Social Web Working Group Charter](https://www.w3.org/2013/socialweb/social-wg-charter) proposes the following **deliverables**, which have been met by the Group, and will be referred to throughout this document:
	  >
	  > **Social Data Syntax -** A JSON-based syntax to allow the transfer of social information, such as status updates, across differing social systems.
	  >
	  > **Social API** - A document that defines a specification for a client-side API 
	  that lets developers embed and format third party information such as 
	  social status updates.
	  >
	  > **Federation Protocol** - A Web protocol to allow the federation of activity-based 
	  status updates and other data (such as profile information) between 
	  heterogeneous Web-based social systems. Federation should include 
	  multiple servers sharing updates within a client-server architecture, 
	  and allow decentralized social systems to be built.
- https://www.w3.org/TR/activitystreams-core/
	- > This specification details a model for representing potential and 
	  completed activities using the JSON format. It is intended to be used 
	  with vocabularies that detail the structure of activities, and define 
	  specific types of activities.
- https://www.w3.org/TR/activitystreams-vocabulary/
	- > This specification describes the Activity vocabulary. It is 
	  intended to be used in the context of the ActivityStreams 2.0 format and
	   provides a foundational vocabulary for activity structures, and 
	  specific activity types.
- https://www.w3.org/TR/activitypub/
	- > The ActivityPub protocol is a decentralized social networking protocol
	          based upon the [[ActivityStreams](https://www.w3.org/TR/activitypub/#bib-ActivityStreams)] 2.0 data format.
	          It provides a client to server API for creating, updating and deleting
	          content, as well as a federated server to server API for delivering
	          notifications and content.
	- ![Actor with messages flowing from rest of world to inbox and from outbox to rest of world](https://www.w3.org/TR/activitypub/illustration/tutorial-2.png){:height 359, :width 746}
-
- ## Related
	- https://www.w3.org/community/socialcg/
	- https://www.w3.org/community/credentials/
	- https://www.w3.org/community/openannotation/
	- https://www.w3.org/community/solid/
	- https://www.w3.org/community/nostr/
	-
-